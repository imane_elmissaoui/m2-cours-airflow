# M2 - Cours Airflow

Data Management Automation with Apache Airflow

This repository contains the files and scripts for automating data management tasks using Apache Airflow. The automation process covers data compilation, cleaning, and archiving for monthly datasets.
Repository Structure

    airflow/: Contains all the design and implementation files for data management automation.
        Datamanagementflowchart.pdf: A flowchart depicting the data management process.
        FluxDag.txt: Textual representation of the DAGs that needed to be implemented for the automation of Data management.
        Notes_Datamanagment.docx: Additional notes and documentation on the data management process.
        dags/: Contains the DAGs and their related scripts.
            Python scripts: Automation scripts for various data management tasks.
            Log files: Execution logs and results, including screenshots of the output.

Problem Statement

Previously, the data management and compilation of new monthly datasets were performed manually. With the recent data export for February 2023, the goal is to automate this process using Airflow.
Expected Outputs

    Cleaned medication, patient stay, and patient files stored in a data warehouse.
    A cleaned monthly export file stored in clean_monthly_export.
    A compiled file consolidating all cleaned data stored in latest_full_export.
    Archiving of the previous compiled file.

Available Resources

    Raw files for December, January, and February.
    Cleaned data for December  Jandanuary.
    A compiled file for data up to January.

Instructions

    Design the Workflow: Organize tasks into DAGs and implement them using Airflow.
    Use Python: Write auxiliary scripts in Python.
    Organize Clearly: Maintain a clear directory structure.
    Log Management: Consider useful logs for tracking the workflow.


Issues

    Output files are not being saved inside the data folder as expected. But when the dag is locally executed the output files 
    are correctly saved inside the folders archive, clean_monthly_export and latest_full_export .

