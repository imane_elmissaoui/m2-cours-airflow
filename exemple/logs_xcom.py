from airflow import DAG
from airflow.operators.bash import BashOperator

from datetime import datetime, timedelta

from airflow.utils.trigger_rule import TriggerRule


def get_time():
    return(str(datetime.now()))

with DAG(
    'exemple_logs_xcom',
    description="Exemple de récupération des logs directement au sein de la tâche + Tâche en cas d'erreur + XCOM pour la date de début de l'exécution",
    schedule_interval = None,
    start_date=datetime(2024, 5, 9),
    catchup = False,
    is_paused_upon_creation = True

) as dag:

    
    t1 = BashOperator(
        task_id='lister_contenu',
        bash_command='ls ')



    t2 = BashOperator(
        task_id='execute_python',
        bash_command="echo '{{ ti.xcom_push(key='start', value='" + str(datetime.now()) + 
            "') }}' ; python3 /opt/airflow/dags/scripts_autres/script_logs_xcom.py" ,
        do_xcom_push = True)

    t3 = BashOperator(
        task_id="echec_execute_python",
        trigger_rule="all_failed",
        bash_command ="python3 /opt/airflow/dags/scripts_autres/failed_logs_xcom.py '{{ti.xcom_pull(key='start')}}'"  
        )


    t1 >>  t2 >> t3