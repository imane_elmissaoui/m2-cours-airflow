# -*- coding: utf-8 -*-
"""
Created on Wed May 29 14:10:47 2024

@author: Imy
"""
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.latest_only_operator import LatestOnlyOperator
from airflow.operators.python import BranchPythonOperator
from datetime import datetime
import os
import pandas as pd

# Define default arguments
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 1, 1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1
}

# Define the DAG
with DAG('REA_monthly_XCOM', default_args=default_args, schedule_interval='@monthly') as dag:

    start = DummyOperator(task_id='start')
    end = DummyOperator(task_id='end')

    latest_only = LatestOnlyOperator(task_id='latest_only')

    # Directory where the data is stored
    data_dir = 'C:/Users/Imy/Desktop/airflow/dags/data'  # Adjust this path accordingly
    log_file_path = 'C:/Users/Imy/Desktop/airflow/dags/logs_XCOM.csv'

    def log_task(task_id, status):
        # Date de début de tâche
        start_time = datetime.now()

        # Si existant, récupérer le fichier de logs précédent
        if os.path.exists(log_file_path):
            logs = pd.read_csv(log_file_path, sep=';', decimal=',')
        else:
            logs = pd.DataFrame({
                'task_id': [],
                'start': [],
                'end': [],
                'status': [],
            })

        # Date de fin de tâche
        end_time = datetime.now()

        # Logs de l'exécution
        new_logs = pd.DataFrame({
            'task_id': [task_id],
            'start': [start_time],
            'end': [end_time],
            'status': [status]
        })

        # Compil avec les précédents logs
        all_logs = pd.concat([logs, new_logs])

        # Exports
        all_logs.to_csv(log_file_path, sep=';', decimal=',', index=False)

    # Function to read and process rea_full CSV files
    def process_rea_full(file_path, **kwargs):
        task_id = 'process_rea_full'
        try:
            print(f"Processing {file_path}")
            df = pd.read_csv(file_path)
            print(df.head())
            # Perform data processing here
            log_task(task_id, 'success')
            return 'success'
        except Exception as e:
            print(f"Failed to process {file_path}: {e}")
            log_task(task_id, 'failure')
            return 'failure'

    # Function to read and process rea_month CSV files
    def process_rea_month(file_path, **kwargs):
        task_id = 'process_rea_month'
        try:
            print(f"Processing {file_path}")
            df = pd.read_csv(file_path)
            print(df.head())
            # Perform data processing here
            log_task(task_id, 'success')
            return 'success'
        except Exception as e:
            print(f"Failed to process {file_path}: {e}")
            log_task(task_id, 'failure')
            return 'failure'

    # Task to process rea_full files
    def process_rea_full_task(**kwargs):
        ti = kwargs['ti']
        for root, dirs, files in os.walk(data_dir):
            for file in files:
                if file.startswith('rea_full') and file.endswith('.csv'):
                    result = process_rea_full(os.path.join(root, file))
                    ti.xcom_push(key=f"rea_full_{file}", value=result)

    # Task to process rea_month files
    def process_rea_month_task(**kwargs):
        ti = kwargs['ti']
        for root, dirs, files in os.walk(data_dir):
            for file in files:
                if file.startswith('rea_month') and file.endswith('.csv'):
                    result = process_rea_month(os.path.join(root, file))
                    ti.xcom_push(key=f"rea_month_{file}", value=result)

    def check_rea_full_status(**kwargs):
        ti = kwargs['ti']
        for root, dirs, files in os.walk(data_dir):
            for file in files:
                if file.startswith('rea_full') and file.endswith('.csv'):
                    result = ti.xcom_pull(key=f"rea_full_{file}", task_ids='process_rea_full_files')
                    if result == 'failure':
                        return 'handle_rea_full_failure'
        return 'all_rea_full_success'

    def check_rea_month_status(**kwargs):
        ti = kwargs['ti']
        for root, dirs, files in os.walk(data_dir):
            for file in files:
                if file.startswith('rea_month') and file.endswith('.csv'):
                    result = ti.xcom_pull(key=f"rea_month_{file}", task_ids='process_rea_month_files')
                    if result == 'failure':
                        return 'handle_rea_month_failure'
        return 'all_rea_month_success'

    def handle_rea_full_failure():
        print("Handling REA full failure")
        log_task('handle_rea_full_failure', 'failed')

    def handle_rea_month_failure():
        print("Handling REA month failure")
        log_task('handle_rea_month_failure', 'failed')

    # Define Python tasks
    process_rea_full_operator = PythonOperator(
        task_id='process_rea_full_files',
        python_callable=process_rea_full_task,
        provide_context=True
    )

    process_rea_month_operator = PythonOperator(
        task_id='process_rea_month_files',
        python_callable=process_rea_month_task,
        provide_context=True
    )

    check_rea_full_status_operator = BranchPythonOperator(
        task_id='check_rea_full_status',
        python_callable=check_rea_full_status,
        provide_context=True
    )

    check_rea_month_status_operator = BranchPythonOperator(
        task_id='check_rea_month_status',
        python_callable=check_rea_month_status,
        provide_context=True
    )

    handle_rea_full_failure_operator = PythonOperator(
        task_id='handle_rea_full_failure',
        python_callable=handle_rea_full_failure
    )

    handle_rea_month_failure_operator = PythonOperator(
        task_id='handle_rea_month_failure',
        python_callable=handle_rea_month_failure
    )

    all_rea_full_success = DummyOperator(task_id='all_rea_full_success')
    all_rea_month_success = DummyOperator(task_id='all_rea_month_success')

    # Set task dependencies
    start >> latest_only >> [process_rea_full_operator, process_rea_month_operator]
    process_rea_full_operator >> check_rea_full_status_operator >> [handle_rea_full_failure_operator, all_rea_full_success]
    process_rea_month_operator >> check_rea_month_status_operator >> [handle_rea_month_failure_operator, all_rea_month_success]
    [all_rea_full_success, all_rea_month_success] >> end
