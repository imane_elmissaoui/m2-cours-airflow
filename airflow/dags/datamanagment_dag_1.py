# -*- coding: utf-8 -*-
"""
Created on Wed May 29 14:10:47 2024

@author: Imy
"""

from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago
from datetime import datetime, timedelta
import os
import pandas as pd

# Define the default arguments for the DAG
default_args = {
    'depends_on_past': False,
    'start_date': days_ago(1),  # Use days_ago for simplicity
    'retry_delay': timedelta(minutes=5),
}

# Define the DAG
dag = DAG('data_management', default_args=default_args, schedule_interval='@monthly')


# Define the function to extract and clean the data
def extract_and_clean_data(data_folder_path, output_file_path, current_month_str):
    # Import the data
    patient_file = f"patient_{current_month_str}.csv"
    sejour_file = f"sejour_{current_month_str}.csv"
    medicament_file = f"medicaments_{current_month_str}.csv"

    patient_df = pd.read_csv(os.path.join(data_folder_path, patient_file), sep=';')
    sejour_df = pd.read_csv(os.path.join(data_folder_path, sejour_file), sep=';')
    medicament_df = pd.read_csv(os.path.join(data_folder_path, medicament_file), sep=';')

    # Data management
    patient = patient_df[['ID_PATIENT', 'SEXE']].rename(columns={'SEXE': 'sexe'})
    sejour = sejour_df[['ID_PATIENT', 'ID_SEJOUR', 'ENTREE_REVEIL', 'SORTIE_REVEIL', 'ENTREE_REA', 'SORTIE_REA']]

    sejour['ENTREE_REVEIL'] = pd.to_datetime(sejour['ENTREE_REVEIL'], format='%d/%m/%Y %H:%M:%S', dayfirst=True)
    sejour['SORTIE_REVEIL'] = pd.to_datetime(sejour['SORTIE_REVEIL'], format='%d/%m/%Y %H:%M:%S', dayfirst=True)
    sejour['ENTREE_REA'] = pd.to_datetime(sejour['ENTREE_REA'], format='%d/%m/%Y %H:%M:%S', dayfirst=True)
    sejour['SORTIE_REA'] = pd.to_datetime(sejour['SORTIE_REA'], format='%d/%m/%Y %H:%M:%S', dayfirst=True)

    # Calculate duree_reveil_tot in hh:mm:ss format
    sejour['duree_reveil_tot'] = sejour['SORTIE_REVEIL'] - sejour['ENTREE_REVEIL']
    sejour['duree_reveil_tot'] = sejour['duree_reveil_tot'].apply(
        lambda x: '{:02d}:{:02d}:{:02d}'.format(x.seconds // 3600, (x.seconds % 3600) // 60, x.seconds % 60) if x >= pd.Timedelta(0) else '0'
    )

    # Calculate duree_rea_tot in hh:mm:ss format
    sejour['duree_rea_tot'] = sejour['SORTIE_REA'] - sejour['ENTREE_REA']
    sejour['duree_rea_tot'] = sejour['duree_rea_tot'].apply(
        lambda x: '{:d} day, {:02d}:{:02d}:{:02d}'.format(x.days, x.seconds // 3600, (x.seconds % 3600) // 60, x.seconds % 60) if x.days == 1 and x >= pd.Timedelta(0) else 
                  '{:d} days, {:02d}:{:02d}:{:02d}'.format(x.days, x.seconds // 3600, (x.seconds % 3600) // 60, x.seconds % 60) if x.days > 1 and x >= pd.Timedelta(0) else 
                  '{:02d}:{:02d}:{:02d}'.format(x.seconds // 3600, (x.seconds % 3600) // 60, x.seconds % 60) if x >= pd.Timedelta(0) else '0'
    )

    medicament = medicament_df[['ID_DROGUE', 'ID_SEJOUR']]
    # Calculate the number of unique medications per stay
    medic_df = medicament.groupby('ID_SEJOUR')['ID_DROGUE'].nunique().reset_index(name='nb_medic')

    # Merge the 3 tables into a rea table
    rea = pd.merge(sejour, patient, on='ID_PATIENT')
    rea = pd.merge(rea, medic_df, on='ID_SEJOUR')
    rea = rea[['ID_SEJOUR', 'duree_reveil_tot', 'duree_rea_tot', 'sexe', 'nb_medic']]

    # Save the result into a csv file
    rea.to_csv(output_file_path, index=False)


# Define the function to save the cleaned data
def save_cleaned_data(execution_date, **kwargs):
    current_month_str = execution_date.strftime('%Y%m')
    data_folder_path = os.path.join('C:/Users/Imy/Desktop/airflow/dags/data', current_month_str)
    output_file_path = os.path.join('C:/Users/Imy/Desktop/airflow/dags/data/clean_monthly_export', f'rea_month_{current_month_str}.csv')
    extract_and_clean_data(data_folder_path, output_file_path, current_month_str)


# Define the function to compile the data
def compile_data(execution_date, **kwargs):
    current_month_str = execution_date.strftime('%Y%m')
    prev_month = (execution_date.replace(day=1) - timedelta(days=1)).replace(day=1)
    prev_month_str = prev_month.strftime('%Y%m')

    input_file_path_1 = os.path.join('C:/Users/Imy/Desktop/airflow/dags/data/clean_monthly_export', f'rea_month_{prev_month_str}.csv')
    input_file_path_2 = os.path.join('C:/Users/Imy/Desktop/airflow/dags/data/clean_monthly_export', f'rea_month_{current_month_str}.csv')
    output_file_path = os.path.join('C:/Users/Imy/Desktop/airflow/dags/data/latest_full_export', f'rea_full_{current_month_str}.csv')

    df_1 = pd.read_csv(input_file_path_1)
    df_2 = pd.read_csv(input_file_path_2)
    df = pd.concat([df_1, df_2], ignore_index=True)
    df.to_csv(output_file_path, index=False)


# Define the function to archive the previous compiled data
def archive_previous_compiled_data(execution_date, **kwargs):
    current_month_str = execution_date.strftime('%Y%m')
    input_file_path = os.path.join('C:/Users/Imy/Desktop/airflowTP/dags/data/latest_full_export', f'rea_full_{current_month_str}.csv')
    output_file_path = os.path.join('C:/Users/Imy/Desktop/airflowTP/dags/data/archive', f'rea_full_{current_month_str}.csv')
    os.replace(input_file_path, output_file_path)


# Define the tasks and their dependencies
save_cleaned_data_task = PythonOperator(
    task_id='save_cleaned_data',
    python_callable=save_cleaned_data,
    provide_context=True,
    dag=dag
)

compile_data_task = PythonOperator(
    task_id='compile_data',
    python_callable=compile_data,
    provide_context=True,
    dag=dag
)

archive_previous_compiled_data_task = PythonOperator(
    task_id='archive_previous_compiled_data',
    python_callable=archive_previous_compiled_data,
    provide_context=True,
    dag=dag
)

save_cleaned_data_task >> compile_data_task >> archive_previous_compiled_data_task
