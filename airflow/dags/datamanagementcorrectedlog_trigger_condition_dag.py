from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator
from airflow.operators.bash_operator import BashOperator
from datetime import datetime
import os


# Define default arguments
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 1, 1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1
}

# Define the DAG
with DAG('REA_monthly_log_trigger_condition', default_args=default_args, schedule_interval='@monthly') as dag:

    start = DummyOperator(task_id='start')
    end = DummyOperator(task_id='end')

    data_dir = "C:/Users/Imy/Desktop/airflow/dags/data"
    log_file_path = "C:/Users/Imy/Desktop/airflow/dags/log_trigger_condition.csv"

    def check_rea_full_status(**kwargs):
        ti = kwargs['ti']
        for root, dirs, files in os.walk(data_dir):
            for file in files:
                if file.startswith('rea_full') and file.endswith('.csv'):
                    result = ti.xcom_pull(key=f"rea_full_{file}", task_ids='process_rea_full_files')
                    if result == 'failure':
                        return 'log_rea_full_failure'
        return 'log_rea_full_success'

    def check_rea_month_status(**kwargs):
        ti = kwargs['ti']
        for root, dirs, files in os.walk(data_dir):
            for file in files:
                if file.startswith('rea_month') and file.endswith('.csv'):
                    result = ti.xcom_pull(key=f"rea_month_{file}", task_ids='process_rea_month_files')
                    if result == 'failure':
                        return 'log_rea_month_failure'
        return 'log_rea_month_success'

    check_rea_full_status_operator = BranchPythonOperator(
        task_id='check_rea_full_status',
        python_callable=check_rea_full_status,
        provide_context=True
    )

    check_rea_month_status_operator = BranchPythonOperator(
        task_id='check_rea_month_status',
        python_callable=check_rea_month_status,
        provide_context=True
    )

    # BashOperators for logging
    log_rea_full_success = BashOperator(
        task_id='log_rea_full_success',
        bash_command="echo 'REA full processing success' >> C:/Users/Imy/Desktop/airflow/dags/log_trigger_condition.csv"
    )

    log_rea_month_success = BashOperator(
        task_id='log_rea_month_success',
        bash_command="echo 'REA month processing success' >> C:/Users/Imy/Desktop/airflow/dags/log_trigger_condition.csv"
    )

    log_rea_full_failure = BashOperator(
        task_id='log_rea_full_failure',
        bash_command="echo 'REA full processing failure' >> C:/Users/Imy/Desktop/airflow/dags/log_trigger_condition.csv"
    )

    log_rea_month_failure = BashOperator(
        task_id='log_rea_month_failure',
        bash_command="echo 'REA month processing failure' >> C:/Users/Imy/Desktop/airflow/dags/log_trigger_condition.csv"
    )

    # Set task dependencies
    start >> check_rea_full_status_operator >> [log_rea_full_failure, log_rea_full_success] >> end
    start >> check_rea_month_status_operator >> [log_rea_month_failure, log_rea_month_success] >> end
