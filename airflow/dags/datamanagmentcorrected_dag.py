# -*- coding: utf-8 -*-
"""
Created on Wed May 29 14:10:47 2024

@author: Imy
"""
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.latest_only_operator import LatestOnlyOperator
from datetime import datetime
import os
import pandas as pd

# Define default arguments
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 1, 1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1
}

# Define the DAG
with DAG('REA_monthly_Datamanagement', default_args=default_args, schedule_interval='@monthly') as dag:

    start = DummyOperator(task_id='start')
    end = DummyOperator(task_id='end')

    latest_only = LatestOnlyOperator(task_id='latest_only')

    # Directory where the data is stored
    data_dir = '/path/to/your/data/directory'  # Adjust this path accordingly

    # Function to read and process rea_full CSV files
    def process_rea_full(file_path):
        print(f"Processing {file_path}")
        df = pd.read_csv(file_path)
        print(df.head())
        # Perform data processing here

    # Function to read and process rea_month CSV files
    def process_rea_month(file_path):
        print(f"Processing {file_path}")
        df = pd.read_csv(file_path)
        print(df.head())
        # Perform data processing here

    # Task to process rea_full files
    def process_rea_full_task():
        for root, dirs, files in os.walk(data_dir):
            for file in files:
                if file.startswith('rea_full') and file.endswith('.csv'):
                    process_rea_full(os.path.join(root, file))

    # Task to process rea_month files
    def process_rea_month_task():
        for root, dirs, files in os.walk(data_dir):
            for file in files:
                if file.startswith('rea_month') and file.endswith('.csv'):
                    process_rea_month(os.path.join(root, file))

    # Define Python tasks
    process_rea_full_operator = PythonOperator(
        task_id='process_rea_full_files',
        python_callable=process_rea_full_task
    )

    process_rea_month_operator = PythonOperator(
        task_id='process_rea_month_files',
        python_callable=process_rea_month_task
    )

    # Set task dependencies
    start >> latest_only >> [process_rea_full_operator, process_rea_month_operator] >> end
