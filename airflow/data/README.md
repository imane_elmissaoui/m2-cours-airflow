# Données à disposition 

- Les dossiers nommés ***230101***, ***230201*** et ***230301*** contiennent les exports respectivement reçus les premier janvier, févirer et mars 2023 et portant sur le mois précédent. Ils contiennent chacun tois fichiers : ***patient_xxxxxx.csv, sejour_xxxxxx.csv*** et ***medicaments_xxxxxx.csv*** 
- ***rea_month_230201.csv*** et ***rea_month_2301001.csv*** les fichiers néttoyés manuellement repsectivement les premiers février et janvier pour les données de janvier et décembre
- rea_full_230201.csv le fichier compilé contenant toutes les données nettoyées de décembre à janvier inclus

# Variables calculées

| **nom** | **Signification** |
|---|---|
| duree_reveil_tot | Durée totale passée en réveil au cours du séjour |
| duree_rea_tot | Durée totale passée en réa au cours du séjour|
| nb_medic | Nombre de médicaments différents administrés au cours du séjour |